export const videos = [
	{
		title: "À partir de 2%",
		subtitle: "Rien à payer avant d'avoir vendu",
		img: require('commons/img/video317a57fc19.jpg'),
		video: 'https://player.vimeo.com/video/196321350'
	},
	{
		title: "100% des acheteurs",
		subtitle: "Pour rejoindre 100% des acheteurs",
		img: require('commons/img/video3983544367.jpg'),
		video: 'https://player.vimeo.com/video/196320920'
	},
	{
		title: "Meilleur des deux mondes",
		subtitle: "À vendre par le proprio... et son courtier",
		img: require('commons/img/video46e71943a6.jpg'),
		video: 'https://player.vimeo.com/video/196320799'
	},
	{
		title: "Capsule Web « Avec ou sans courtier? »",
		subtitle: "",
		img: require('commons/img/video873883b9df.jpg'),
		video: 'https://player.vimeo.com/video/196320998'
	},
	{
		title: "Anglais #1",
		subtitle: "For sale by the owner... and their broker.",
		img: require('commons/img/video141201517.jpg'),
		video: 'https://player.vimeo.com/video/151647128'
	},
	{
		title: "Anglais #2",
		subtitle: "100% of buyers",
		img: require('commons/img/video141201515.jpg'),
		video: 'https://player.vimeo.com/video/151647127'
	},
	{
		title: "Anglais #3",
		subtitle: "2%",
		img: require('commons/img/video141201469.jpg'),
		video: 'https://player.vimeo.com/video/151647129'
	}
];