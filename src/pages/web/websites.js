export const websites = [
	{title: "Programme Visibili&#8209;T", img: require('commons/img/webprogrammevisibilit.jpg')},
	{title: "Re&#8209;targeting", img: require('commons/img/webretargeting.jpg')},
	{title: "Page de destination", img: require('commons/img/webdestination.jpg')},
	{title: "Référencement", img: require('commons/img/webreferencement.jpg')},
	{title: "Médias sociaux", img: require('commons/img/webmediasociaux.jpg')},	
];