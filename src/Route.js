import _ from 'lodash'
import VueRouter from 'vue-router'
import Settings from 'config/Settings'
import DefaultLanguage from 'core/js/DefaultLanguage'
import KFFEIN_map from 'json!./Route.json'
import configBuild from 'root/config/index.js'

const modeBuild = (process.env.NODE_ENV === 'production') ? 'build' : 'dev'

let map = []

// on format KFFEIN_map pour qu'il ressemble a  
// http://vuejs.github.io/vue-router/en/named.html
_.forEach(Settings.locale, lang => {
  let prefix = (Settings.prefixRoute) ? '/' + lang : ''
  _.forEach(KFFEIN_map, (route, key) => {
    map.push({ 
      path: prefix + route[lang], 
      name: key + '.' + lang, 
      meta: {
        name: key,
        lang: lang
      },
      component: require('src/' + route.component + '.vue')
    })
  })
})

// les routes doivent etre unique
if(map.length !== _.uniqBy(map, 'path').length)
  throw new Error('DUPLICATE ROUTE FOUND, CHECK Route.js')

// redirection sur la home (lang detect) + 404 
map.push({ path: '/', redirect: { name: 'home.' + DefaultLanguage }})
map.push({ path: '*', redirect: { name: '404.' + DefaultLanguage }})

const router = new VueRouter({ 
  mode: 'hash',
  base: configBuild[modeBuild].assetsPublicPath,
  routes: map,
})

export default router
