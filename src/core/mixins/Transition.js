import Settings from 'config/Settings'

export default {
  computed: {
    mode() { 
      return this.$store.getters.mode
    }
  },
  methods: {
    afterEnter() {
      if(this.mode == 'in-out' && this.mode != Settings.mode)
        this.$store.commit('app/RESET_MODE')
    },
    afterLeave() {
      if(this.mode == 'out-in' || this.mode == '' && this.mode != Settings.mode)
        this.$store.commit('app/RESET_MODE')
    },
    enter(el, done) {
      this.$refs.page.enter
      ? this.$refs.page.enter(done)
      : TweenMax.from(el, .5, { alpha: 0, onComplete: done })
    },
    leave(el, done) {
      this.$prevLeave
      ? this.$prevLeave(done)
      : TweenMax.to(el, .5, { alpha: 0, onComplete: done })
    }
  }
} 
