import BrowserLanguage from 'in-browser-language'
import Cookies from 'js-cookie'
import Settings from 'config/Settings'
import _ from 'lodash'

// Cookies.remove(Settings.cookies.lang)

const defaultLanguage = Cookies.get(Settings.cookies.lang) || BrowserLanguage.pick(Settings.locale, _.head(Settings.locale)) 

export default defaultLanguage
