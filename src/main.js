import _ from 'lodash'
import Vue from 'vue'
import App from 'src/App'
import Axios from 'axios'
import VueRouter from 'vue-router'
import store from 'vuex/Store'
import router from 'Route'
import VueHead from 'core/plugins/vue-head'
import VueLazyload from 'vue-lazyload'
import * as CommonsTranslations from 'commons/lang/Index.js'
import Settings from 'config/Settings'
import Translation from 'core/mixins/Translation'
import Agent from 'core/js/Agent'
import 'core/js/Copyright'

Vue.prototype.$http = Axios.create()
Vue.use(VueRouter)
Vue.use(VueHead)
Vue.use(VueLazyload)
Vue.mixin(Translation)

let locales = {}
_.each(CommonsTranslations, o => _.merge(locales, o))

new Vue({
  el: '#App',
  store,
  router,
  head: {
    title() {
      return {
        inner: this.trans('Global.meta.title')
      }
    },
    meta() {
      return [
        { name: 'description', content: this.trans('Global.meta.description'), id: 'description' }
      ]
    },
  },
  data: () => ({
    $agent: null
  }),
  beforeCreate() {
    this.$store.commit('app/LANG_CHANGED', this.$route.meta.lang)
    this.$store.commit('app/SET_FROM_ROUTE', this.$route)
    this.$store.commit('app/SET_TO_ROUTE', this.$route)
  },
  created() {
    this.$agent = Agent
    this.setLangRouteNames()
  },
  computed: {
    commonsTranslations: {
      get: () => locales,
      set: o => _.merge(locales, o)
    },
  },
  methods: {
    langSwitch(lang) {
      if(lang === undefined) {
        if(Settings.locale.length > 2)
          throw new Error('Please specify the lang')

        lang = _.without(Settings.locale, this.lang)
      }

      this.$router.replace({ 
        name: this.trans('Route.name', lang),
        params: this.trans('Route.params', lang)
      })
    },
    setLangRouteNames() {
      let langRouteNames = {}

      Settings.locale.forEach(lang => {
        langRouteNames[lang] = { 
          Route: { name: this.$route.meta.name + '.' + lang, params: null }
        } 
      })

      this.commonsTranslations = langRouteNames
    }
  },
  render: h => h(App)
})

