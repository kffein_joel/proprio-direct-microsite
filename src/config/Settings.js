var _ = require('lodash')

// global
var Settings = {
  name: 'KODDEIN VUEJS',
  id: 'koddein_vuejs',
  cookies: {
    lang: 'koddein_vuejs_lang',
  },
  locale: ['fr'],
  loader: false,
  // can be 'out-in', 'in-out' or ''
  mode: 'out-in',
  // prefix les routes avec les lang disponible
  // ex : fr/accueil
  prefixRoute: false,
  // Si vuejs est utiliser avec un CMS (craft ou wordpress)
  cms: false,
  // build le projet dans une sous-repertoire
  subfolder: '',
  API_PREFIX: 'api',
  ADMIN_PREFIX: 'admin'
}

// dev
if((process.env.NODE_ENV !== 'production'))
{
  Settings['BASE_URL'] = ''
}
// production
else
{
  Settings['BASE_URL'] = ''
}

if(typeof(window) !== 'undefined') {
  // disable loader for prerender.io
  if (/PhantomJS/.test(window.navigator.userAgent)) 
    Settings.loader = false

  // OVERWRITE BY PHP ENV
  if(typeof(window) !== 'undefined' && window.__ENV !== undefined)
    Settings = _.merge(Settings, window.__ENV)
}

// concat BASE_URL + PREFIX
Settings['API_URL'] = Settings.BASE_URL + '/' + Settings.API_PREFIX + '/'


module.exports = Settings

