import Vue from 'vue'
import Vuex from 'vuex'
import DefaultLanguage from 'core/js/DefaultLanguage'
import Settings from 'config/Settings'
import Cookies from 'js-cookie'

Vue.use(Vuex)

const route = {
  name: null,
  meta: { 
    lang: DefaultLanguage, 
    name: null
  }
}

const state = {
  lang: DefaultLanguage,
  loaded: !Settings.loader,
  promises: [],
  mode: Settings.mode,
  route: {
    from: route,
    to: route
  }
}

const mutations = {
  'app/SET_MODE' (state, mode) {
    state.mode = mode
  },
  'app/RESET_MODE' (state) {
    state.mode = Settings.mode
  },
  'app/SET_FROM_ROUTE' (state, from) {
    state.route.from = from
  },
  'app/SET_TO_ROUTE' (state, to) {
    state.route.to = to
  },
  'app/LANG_REFRESH' (state) {
    const l = state.lang
    state.lang = 'none'
    state.lang = l
  },
  'app/LANG_CHANGED' (state, lang) {
    state.lang = lang
    Cookies.set(Settings.cookies.lang, state.lang)
    Vue.prototype.$http.defaults.baseURL = Settings.API_URL + state.lang
    document.getElementsByTagName('html')[0].setAttribute('lang', state.lang)
  },
  'loader/LOADER_LOADED' (state) {
    state.loaded = true
  },
  'loader/PROMISE_PUSH' (state, promise) {
    if(state.promises)
      state.promises.push(promise)
  },
  'loader/PROMISE_DEACTIVATE' (state) {
    state.promises = null
  },
}

const getters = {
  lang: state => state.lang,
  loaded: state => state.loaded,
  promises: state => state.promises,
  mode: state => state.mode,
  route: state => state.route,
}

export default new Vuex.Store({
  state,
  getters,
  mutations
})
