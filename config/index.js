// see http://vuejs-templates.github.io/webpack for documentation.
var path = require('path')
var configVuejs = require(__dirname + '/../src/config/Settings.js')
var _ = require('lodash')

var configBuild = {
  build: {
    env: require('./prod.env'),

    index: path.resolve(__dirname, '../dist', configVuejs.subfolder, 'index.html'),
    assetsRoot: path.resolve(__dirname, '../dist', configVuejs.subfolder),
    assetsPublicPath: path.join('/', configVuejs.subfolder, '/'),
    assetsSubDirectory: 'static',
    productionSourceMap: false,
    // Gzip off by default as many popular static hosts such as
    // Surge or Netlify already gzip all static assets for you.
    // Before setting to `true`, make sure to:
    // npm install --save-dev compression-webpack-plugin
    productionGzip: false,
    productionGzipExtensions: ['js', 'css']
  },
  dev: {
    env: require('./dev.env'),
    port: 8080,
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    proxyTable: {},
    // CSS Sourcemaps off by default because relative paths are "buggy"
    // with this option, according to the CSS-Loader README
    // (https://github.com/webpack/css-loader#sourcemaps)
    // In our experience, they generally work as expected,
    // just be aware of this issue when enabling this option.
    cssSourceMap: false
  }
}

if(configVuejs.cms) {
  configBuild.build.index = path.resolve(__dirname, '../../public/app.html')
  configBuild.build.assetsRoot = path.resolve(__dirname, '../../public')
  _.each(['/' + configVuejs.API_PREFIX, '/' + configVuejs.ADMIN_PREFIX, '/actions', '/cpresources'], o => {
    configBuild.dev.proxyTable[o] = {
      target: configVuejs.BASE_URL,
      changeOrigin: true,
    }
  })
}

module.exports = configBuild

