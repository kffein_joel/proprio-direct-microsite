# Koddein Vuejs 2.0

[Documentation](https://bitbucket.org/kffein/koddein-vuejs/wiki/Home)

## Subfolder apache

```
RewriteEngine On  
  # If an existing asset or directory is requested go to it as it is
  RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -f [OR]
  RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -d
  RewriteRule ^ - [L]

  # If the requested resource doesn't exist, use index.html
  RewriteRule ^ /CHANGE_BY_YOUR_SUBFOLDER/index.html

```
