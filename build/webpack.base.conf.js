var path = require('path')
var config = require('../config')
var utils = require('./utils')
var projectRoot = path.resolve(__dirname, '../')

var env = process.env.NODE_ENV
// check env & config/index.js to decide weither to enable CSS Sourcemaps for the
// various preprocessor loaders added to vue-loader at the end of this file
var cssSourceMapDev = (env === 'development' && config.dev.cssSourceMap)
var cssSourceMapProd = (env === 'production' && config.build.productionSourceMap)
var useCssSourceMap = cssSourceMapDev || cssSourceMapProd

var webpack = require('webpack')
var SpritesmithPlugin = require('webpack-spritesmith')
var ManifestPlugin = require('webpack-manifest-plugin')

module.exports = {
  entry: {
    app: './src/main.js'
  },
  output: {
    path: config.build.assetsRoot,
    publicPath: process.env.NODE_ENV === 'production' ? config.build.assetsPublicPath : config.dev.assetsPublicPath,
    filename: '[name].js'
  },
  externals: {
    'TweenLite': 'TweenLite',
    'TweenMax': 'TweenMax',
    'TimelineMax': 'TimelineMax',
    'TimelineLite': 'TimelineLite',
    'CSSPlugin': 'CSSPlugin'
  },
  resolve: {
    extensions: ['', '.js', '.vue'],
    fallback: [path.join(__dirname, '../node_modules')],
    alias: {
      'vue$': 'vue/dist/vue.common.js',
      'root': path.resolve(__dirname, '../'),
      'src': path.resolve(__dirname, '../src'),
      'assets': path.resolve(__dirname, '../src/assets'),
      'components': path.resolve(__dirname, '../src/components')
    },
    modulesDirectories: ["node_modules", "src"]
  },
  resolveLoader: {
    fallback: [path.join(__dirname, '../node_modules')]
  },
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue'
      },
      {
        test: /\.js$/,
        loader: 'babel',
        include: projectRoot,
        exclude: /node_modules/
      },
      {
        test: /\.json$/,
        include: path.join(__dirname, 'node_modules'),
        loader: 'json'
      },
      { 
        test: /\/font.*.(woff(2)?|eot|ttf|svg)(\?[a-z0-9=\.]+)?$/, 
        loader: "file-loader?name=" + path.join(config.build.assetsSubDirectory, 'fonts/[name].[hash:7].[ext]') 
      },
      {
        test: /\/sprite.*.svg$/,
        loader: 'svg-sprite?' + JSON.stringify({
          prefixize: true,
          name: '[name].[hash:7].[ext]'
        })
      },
      {
        test: /\.(ico)$/,
        loader: 'url',
        query: {
          limit: 1,
          name: path.join(config.build.assetsSubDirectory, 'ico/[name].[hash:7].[ext]')
        }
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'url',
        query: {
          limit: 1,
          name: path.join(config.build.assetsSubDirectory, 'img/[name].[hash:7].[ext]')
        }
      },
      {
        test: /\.(webm|mp4)$/,
        loader: 'url',
        query: {
          limit: 1,
          name: path.join(config.build.assetsSubDirectory, 'video/[name].[hash:7].[ext]')
        }
      }
    ]
  },
  vue: {
    loaders: utils.cssLoaders({ sourceMap: useCssSourceMap }),
    postcss: [
      require('lost')(),
      require('autoprefixer')({ 
        browsers: ['last 5 versions'] 
      }),
      require('postcss-aspect-ratio')
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      'Promise': 'es6-promise',
    }),
    new ManifestPlugin(),
    new SpritesmithPlugin({
      src: {
        cwd: path.resolve(__dirname, '../src'),
        glob: '**/sprite/*.png'
      },
      target: {
        image: path.resolve(__dirname, '../src/._trash/sprite.png'),
        css: [path.resolve(__dirname, '../src/._trash/Sprite.styl')]
      },
      apiOptions: {
        cssImageRef: '~._trash/sprite.png'
      }
    })
  ]
}
